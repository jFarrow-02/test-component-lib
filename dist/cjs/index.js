'use strict';

var React = require('react');

function styleInject(css, ref) {
  if ( ref === void 0 ) ref = {};
  var insertAt = ref.insertAt;

  if (!css || typeof document === 'undefined') { return; }

  var head = document.head || document.getElementsByTagName('head')[0];
  var style = document.createElement('style');
  style.type = 'text/css';

  if (insertAt === 'top') {
    if (head.firstChild) {
      head.insertBefore(style, head.firstChild);
    } else {
      head.appendChild(style);
    }
  } else {
    head.appendChild(style);
  }

  if (style.styleSheet) {
    style.styleSheet.cssText = css;
  } else {
    style.appendChild(document.createTextNode(css));
  }
}

var css_248z = ".Button-module_my-button__Sa5Jj {\n  color: #fff;\n  background-color: blue;\n  border: none;\n  border-radius: 8px;\n  padding: 25px;\n}\n";
var classes = {"my-button":"Button-module_my-button__Sa5Jj"};
styleInject(css_248z);

const Button = (props) => {
    const { displayText } = props;
    return (React.createElement("button", { type: "button", className: classes['my-button'] }, displayText));
};

exports.Button = Button;
//# sourceMappingURL=index.js.map
