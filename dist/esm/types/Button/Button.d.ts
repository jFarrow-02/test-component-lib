/// <reference types="react" />
import { IButtonProps } from './IButtonProps';
export declare const Button: (props: IButtonProps) => JSX.Element;
