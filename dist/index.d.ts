/// <reference types="react" />
interface IButtonProps {
    displayText: string;
}

declare const Button: (props: IButtonProps) => JSX.Element;

export { Button };
