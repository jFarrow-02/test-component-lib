import React from 'react';
import { IButtonProps } from './IButtonProps';
import classes from './Button.module.css';

export const Button = (props: IButtonProps) => {
  const { displayText } = props;

  return (
    <button type="button" className={classes['my-button']}>
      {displayText}
    </button>
  );
};
